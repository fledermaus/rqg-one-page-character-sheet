# Single Page RQG Character Sheet

Runequest can be a bit crunchy, so I've automated a lot of the mechanics
in a self contained web page.

It makes no connections to outside servers and sets no cookies.

NB: I _may_ introduce a store for character data to make moving from
machine to machine less inconvenient but no such thing exists yet.

Your character data are stored locally in your web browser (so visiting the
same page from a different machine or even a different browser will not reach
the same character)

You may change the page colour scheme by clicking on the palette icon,

Your character data may be exported and imported as text blobs (see the little
floppy disk icons on the page).

The page loads only the following resources, all shipped with the page itself:

## JS
 - glorantha-character.js

## Fonts
 - glorantha-fonts/FreeSans.otf or glorantha-fonts/FreeSans.ttf
 - glorantha-fonts/gloranthan-runes.woff or glorantha-fonts/gloranthan-runes.ttf
 - glorantha-fonts/NotoColorEmoji.ttf and/or glorantha-fonts/NotoEmoji-Regular.ttf

## Images
 - silhouette-m.svg
 - silhouette-f.svg
